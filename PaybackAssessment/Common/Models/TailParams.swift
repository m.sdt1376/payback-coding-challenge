//
//  TailParams.swift
//  PaybackAssessment
//
//  Created by mohammad on 6/2/21.
//

import Foundation

struct TailParams: Encodable {
    // MARK: - Properties
    let alt: String = "media"
    let token: String = "0f3f9a33-39df-4ad2-b9df-add07796a0fa"
}
