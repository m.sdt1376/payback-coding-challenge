//
//  HomeLogger.swift
//  PaybackAssessment
//
//  Created by mohammad on 6/2/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

class HomeLogger: Logger {
    override class var prefix: String {
        return "Home"
    }
}
