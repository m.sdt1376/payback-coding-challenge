//
//  ImageDetailLogger.swift
//  PaybackAssessment
//
//  Created by mohammadSaadat on 3/14/1400 AP.
//  Copyright (c) 1400 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

class ImageDetailLogger: Logger {
    override class var prefix: String {
        return "ImageDetail"
    }
}
