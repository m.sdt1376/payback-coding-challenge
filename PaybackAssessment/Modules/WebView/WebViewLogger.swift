//
//  WebViewLogger.swift
//  TRB
//
//  Created by Farzad on 8/19/20.
//  Copyright (c) 2020 RoundTableApps. All rights reserved.
//

import Foundation

class WebViewLogger: Logger {
    override class var prefix: String {
        return "WebView"
    }
}
